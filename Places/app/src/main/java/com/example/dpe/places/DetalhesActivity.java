package com.example.dpe.places;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpe.places.Models.ImageHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetalhesActivity extends AppCompatActivity {

    String api_key = "AIzaSyDIHZo3Cqho2ls4vw4QcV7SQcJgN7TCZgU";
    String url = "https://maps.googleapis.com/maps/api/place/details/"+
            "json?key=" + api_key + "&language=pt-BR&placeid=";
    String url_image = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=" + api_key + "&photoreference=";
    String placeId, imageUrl;
    List<HashMap<String, String>> arrayList = new ArrayList<>();
    TextView txtNomeDetalhes, txtEnderecoDetalhes;
    ListView listViewComentarios;
    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Detalhes");

        txtNomeDetalhes = findViewById(R.id.txtNomeDetalhes);
        txtEnderecoDetalhes = findViewById(R.id.txtEnderecoDetalhes);
        listViewComentarios = findViewById(R.id.listViewComentarios);
        imgView = findViewById(R.id.imgViewDetalhes);

        placeId = getIntent().getStringExtra("placeId");
        imageUrl = getIntent().getStringExtra("imageUrl");

        sendRequest(url + placeId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:break;
        }
        return true;
    }

    Response.Listener<JSONObject> jsonArrayListener = new
            Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String nome, coment, imagem = "", rating;
                        JSONObject info = response.getJSONObject("result");
                        txtNomeDetalhes.setText(info.getString("name"));
                        txtEnderecoDetalhes.setText(info.getString("formatted_address"));
                        if (!info.isNull("photos"))
                            imagem=url_image + info.getJSONArray("photos").getJSONObject(0).getString("photo_reference");
                        JSONArray reviews = info.getJSONArray("reviews");
                        for(int i=0;i<reviews.length();i++){
                            JSONObject comentario = reviews.getJSONObject(i);
                            nome = comentario.getString("author_name");
                            coment = comentario.getString("text");
                            imagem = comentario.getString("profile_photo_url");
                            rating = String.valueOf(comentario.getDouble("rating"));
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("nome", nome);
                            hashMap.put("comentario", coment);
                            hashMap.put("imagem", imagem);
                            hashMap.put("rating", rating);
                            arrayList.add(hashMap);
                        }
                        listViewComentarios.setAdapter(new ComentarioAdapter(DetalhesActivity.this, arrayList));
                        new ImageHelper(imgView).execute(imageUrl);
                    }catch (JSONException e){
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(), "Ocorreu um erro "+"parsing JSON",Toast.LENGTH_LONG).show();
                    }
                }
            };

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getBaseContext(),"Não é possível conectar ao servidor", Toast.LENGTH_LONG).show();
        }
    };

    private void sendRequest(String urlQuery){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(urlQuery, null, jsonArrayListener, errorListener);
        Volley.newRequestQueue(DetalhesActivity.this).add(jsonObjectRequest);
    }
}
