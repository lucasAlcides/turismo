package com.example.dpe.places;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dpe.places.Models.ImageHelper;
import com.example.dpe.places.Models.Place;

import java.util.List;

public class FavoritosAdapter extends ArrayAdapter<Place> {
    private final Context context;
    private final List<Place> elementos;

    public FavoritosAdapter(Context context, List<Place> elementos) {
        super (context, R.layout.row_favoritos, elementos);
        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_favoritos, parent, false);

        TextView nome = (TextView) rowView.findViewById(R.id.txtNomeLocal);
        TextView endereco = (TextView) rowView.findViewById(R.id.txtEndereco);
        ImageView imagem = (ImageView) rowView.findViewById(R.id.imgFavorito);

        nome.setText(elementos.get(position).getName());
        endereco.setText(String.valueOf(elementos.get(position).getEndereco()));
        new ImageHelper(imagem).execute(elementos.get(position).getUrlImage());

        return rowView;
    }
}
