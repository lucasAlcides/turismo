package com.example.dpe.places.Repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CrudSqLite extends SQLiteOpenHelper {
    private static final String DBNAME = "Turismo";
    private static final int VERSAO = 1;
    private Context _context;

    public CrudSqLite(Context context) {
        super(context,DBNAME,null, VERSAO);
        _context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PlacesRepository.TB_LOCAL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
