package com.example.dpe.places;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dpe.places.Models.Place;
import com.example.dpe.places.Repository.PlacesRepository;

import java.util.List;

public class FavoritosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle saverInstanceState) {
        super.onCreate(saverInstanceState);
        setContentView(R.layout.activity_favoritos);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Favoritos");

        ListView lvFavoritos = findViewById(R.id.listViewFavoritos);

        final List<Place> places = new PlacesRepository(FavoritosActivity.this).FindAll();

        lvFavoritos.setAdapter(new FavoritosAdapter(FavoritosActivity.this, places));

        lvFavoritos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(FavoritosActivity.this, DetalhesActivity.class);
                intent.putExtra("placeId", places.get(position).getIdPlace());
                intent.putExtra("imageUrl", places.get(position).getUrlImage());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:break;
        }
        return true;
    }
}