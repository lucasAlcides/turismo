package com.example.dpe.places;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dpe.places.Models.ImageHelper;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class ComentarioAdapter extends ArrayAdapter<HashMap<String, String>> {
    List<HashMap<String, String>> list;
    Context context;

    public ComentarioAdapter(Context context, List<HashMap<String, String>> list) {
        super(context, R.layout.row_comentario, list);
        this.list = list;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_comentario, parent, false);
        HashMap<String, String> hashMap = list.get(position);

        ImageView imgView = v.findViewById(R.id.imgPessoa);
        new ImageHelper(imgView).execute(hashMap.get("imagem"));
        ((TextView)v.findViewById(R.id.txtNomePessoa)).setText(hashMap.get("nome"));
        ((TextView)v.findViewById(R.id.txtComentarioPessoa)).setText(hashMap.get("comentario"));
        RatingBar ratingBar = v.findViewById(R.id.ratingBarComentario);
        ratingBar.setRating(Float.parseFloat(hashMap.get("rating")));
        ratingBar.setEnabled(false);

        return v;
    }
}
