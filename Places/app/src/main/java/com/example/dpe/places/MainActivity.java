package com.example.dpe.places;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpe.places.Models.Place;
import com.example.dpe.places.Repository.PlacesRepository;
import com.example.dpe.places.databinding.ActivityMainBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding bind;
    ArrayList<HashMap<String, String>> arrayList =
            new ArrayList<HashMap<String, String>>();
    CustomListAdapter adapter;
    String api_key = "AIzaSyDIHZo3Cqho2ls4vw4QcV7SQcJgN7TCZgU";
    String url = "https://maps.googleapis.com/maps/api/place/textsearch/"+
            "json?key=" + api_key + "&language=pt-BR&query=point+of+interest+";
    String url_image = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&key=" + api_key + "&photoreference=";
    String urlQuery ="";
    @Override
    protected void onCreate(Bundle saverInstanceState) {
        super.onCreate(saverInstanceState);
        bind = DataBindingUtil.setContentView(this, R.layout.activity_main);
        adapter = new CustomListAdapter(arrayList, MainActivity.this);
        bind.lvLocais.setAdapter(adapter);
        bind.btnBuscar.setOnClickListener(btnClick);
        bind.btnLimpar.setOnClickListener(btnClick);

        bind.lvLocais.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                openAddFavoritosDialog(position);
                return false;
            }
        });

        bind.lvLocais.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,DetalhesActivity.class);
                intent.putExtra("placeId", arrayList.get(position).get("place_id"));
                intent.putExtra("imageUrl", arrayList.get(position).get("imagem"));
                startActivity(intent);
            }
        });

        getSupportActionBar().setTitle("Pontos");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.favoritos_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorito_menu:
                startActivity(new Intent(this, FavoritosActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    Response.Listener<JSONObject> jsonArrayListener = new
            Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String nome="", estabelecimento="", imagem="", placeId = "", rating = "";
                        arrayList.clear();
                        JSONArray places = response.getJSONArray("results");
                        for(int i=0;i<places.length();i++){
                            JSONObject info = places.getJSONObject(i);
                            nome=info.getString("name");
                            estabelecimento=info.getString("formatted_address");
                            if (!info.isNull("photos"))
                                imagem=url_image + info.getJSONArray("photos").getJSONObject(0).getString("photo_reference");
                            placeId=info.getString("place_id");
                            rating=String.valueOf(info.getDouble("rating"));
                            HashMap<String, String> hashMap = new HashMap<String, String>();
                            hashMap.put("nome", nome);
                            hashMap.put("estabelecimento", estabelecimento);
                            hashMap.put("imagem", imagem);
                            hashMap.put("place_id", placeId);
                            hashMap.put("rating", rating);
                            arrayList.add(hashMap);
                        }
                        adapter.notifyDataSetChanged();
                    }catch (JSONException e){
                        e.printStackTrace();
                        Toast.makeText(getBaseContext(), "Ocorreu um erro "+"parsing JSON",Toast.LENGTH_LONG).show();
                    }
                }
            };

    Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(getBaseContext(),"Não é possível conectar ao servidor", Toast.LENGTH_LONG).show();
        }
    };

    View.OnClickListener btnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btn_buscar:
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(bind.edtBuscar.getWindowToken(), 0);
                    try {
                        urlQuery =url + URLEncoder.encode(bind.edtBuscar.getText().toString(),"UTF-8");
                    }catch (UnsupportedEncodingException e){
                        e.printStackTrace();
                    }
                    sendRequest(urlQuery);
                    break;
                case R.id.btn_limpar:
                    bind.edtBuscar.setText("");
                    arrayList.clear();
                    adapter.notifyDataSetChanged();
            }
        }
    };

    private void sendRequest(String urlQuery){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(urlQuery, null, jsonArrayListener, errorListener);
        Volley.newRequestQueue(MainActivity.this).add(jsonObjectRequest);
    }

    private void openAddFavoritosDialog(int position){
        final int positionId = position;

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Adicionar nos favoritos");

        builder.setMessage("Deseja salvar nos favoritos?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                new PlacesRepository(MainActivity.this).Insert(fillPlace(positionId));
                Toast.makeText(MainActivity.this, "Salvo com sucesso!", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                return;
            }
        });

        AlertDialog alerta = builder.create();

        alerta.show();
    }

    private Place fillPlace(int position){
        Place place = new Place();
        place.setIdPlace(arrayList.get(position).get("place_id"));
        place.setName(arrayList.get(position).get("nome"));
        place.setEndereco(arrayList.get(position).get("estabelecimento"));
        place.setUrlImage(arrayList.get(position).get("imagem"));
        return place;
    }
}
