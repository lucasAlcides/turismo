package com.example.dpe.places;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dpe.places.Models.ImageHelper;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class CustomListAdapter extends BaseAdapter {
    ArrayList<HashMap<String, String>> arrayList;
    AppCompatActivity appCompatActivity;
    LayoutInflater layoutInflater;

    public CustomListAdapter(ArrayList<HashMap<String, String>> arrayList,
                             AppCompatActivity appCompatActivity) {
        this.arrayList = arrayList;
        this.appCompatActivity = appCompatActivity;
        layoutInflater = (LayoutInflater) this.appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        v = layoutInflater.inflate(R.layout.row_detalhes, viewGroup, false);
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap = arrayList.get(i);
        ((TextView) v.findViewById(R.id.txt_nome)).setText(hashMap.get("nome"));
        ((TextView) v.findViewById(R.id.txt_endereco)).setText(hashMap.get("estabelecimento"));
        RatingBar rating =  ((RatingBar) v.findViewById(R.id.ratingBarDetalhes));
        rating.setRating(Float.parseFloat(hashMap.get("rating")));
        rating.setEnabled(false);
        ImageView imgPlace = (ImageView) v.findViewById(R.id.img_place);
        new ImageHelper(imgPlace).execute(hashMap.get("imagem"));
        return v;
    }
}
