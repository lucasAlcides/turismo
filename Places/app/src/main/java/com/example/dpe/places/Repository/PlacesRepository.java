package com.example.dpe.places.Repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dpe.places.Models.Place;

import java.util.ArrayList;
import java.util.List;

public class PlacesRepository {
    public static final String TB_LOCAL = "CREATE TABLE Places (" +
            "    id   INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "    idPlace TEXT," +
            "    name TEXT," +
            "    address TEXT," +
            "    urlImage TEXT);";

    private final String TB_DESCRICAO = "Places";
    private final String[] COLUMNS = { "id" , "idPlace", "name", "address", "urlImage"};


    private CrudSqLite _dbHelper;
    private SQLiteDatabase _db;

    public PlacesRepository(Context context) {
        this._dbHelper = new CrudSqLite(context);
        this._db = _dbHelper.getReadableDatabase();
    }

    public void Insert(Place place){
        _db.insert(TB_DESCRICAO, null, getContentValues(place));
    }

    public List<Place> FindAll(){
        List<Place> listPlaces = new ArrayList<>();
        Cursor cursor = _db.rawQuery("SELECT * FROM " + TB_DESCRICAO, null);
        if (cursor.moveToFirst()){
            do{
                listPlaces.add(FillPlace(cursor));
            }while(cursor.moveToNext());
        }
        return listPlaces;
    }

    public Place FindById(String id){
        Place place = new Place();
        Cursor cursor = _db.query(TB_DESCRICAO, COLUMNS, "idPlace=?", new String[] { String.valueOf(id)}, null, null, "idPlace");
        if (cursor.moveToFirst())
            place = FillPlace(cursor);
        return place;
    }

    private Place FillPlace(Cursor cursor){
        Place place = new Place();
        place.setId(cursor.getInt(cursor.getColumnIndex("id")));
        place.setIdPlace(cursor.getString(cursor.getColumnIndex("idPlace")));
        place.setEndereco(cursor.getString(cursor.getColumnIndex("address")));
        place.setName(cursor.getString(cursor.getColumnIndex("name")));
        place.setUrlImage(cursor.getString(cursor.getColumnIndex("urlImage")));
        return place;
    }

    private ContentValues getContentValues(Place place){
        ContentValues values = new ContentValues();
        values.put("idPlace", place.getIdPlace());
        values.put("name", place.getName());
        values.put("address", place.getEndereco());
        values.put("urlImage", place.getUrlImage());
        return values;
    }
}
